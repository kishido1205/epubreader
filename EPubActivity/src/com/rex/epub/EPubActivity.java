package com.rex.epub;

import java.io.File;
import java.io.IOException;
import java.util.List;

import nl.siegmann.epublib.domain.TOCReference;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import com.rex.epub.db.EPubBookmark;
import com.rex.epub.db.EPubDataSource;
import com.rex.epub.db.EPubNote;
import com.rex.epub.model.EPubBookReader;
import com.rex.epub.widget.EPubReaderView;
import com.rex.epub.widget.EPubReaderView.onReaderViewSwipeListener;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class EPubActivity extends Activity {

    public String bookName;
    
    public EPubBookReader bookReader;
    
    public EPubReaderView webView;
    
    public EPubDataSource dataSource;
    
    final Handler handler = new Handler();
    
    private class ReaderWebClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			bookReader.validate(url);
	       
	        ((TextView) findViewById(R.id.textPage)).setText(
	        		(bookReader.getCurrentPageNumber()+1) + "/" + bookReader.getNumPages());
			return true;
		}
	}

	
	@SuppressLint("SetJavaScriptEnabled")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_epub);
        
        bookName = getIntent().getStringExtra("filename");
        
        if (bookName == null) {
//            bookFilename = "/widget-quiz-20121022.epub"; EPUB/
//        	bookFilename = "/mymedia_lite-20130621.epub"; //OEPBS/text
//        	bookFilename = "/haruko-html-jpeg-20120524.epub"; // OPS/xhtml
        	bookName = "/moby-dick-mo-20120214.epub"; ///OPS/
        }
        
        File book = loadFile(bookName);
        
        if (book.exists()) {
            bookReader = new EPubBookReader(book);
            
            try {
				String data = new String(bookReader.getPage());

	            webView = (EPubReaderView) findViewById(R.id.webview);
	            webView.setWebViewClient(new ReaderWebClient());
	            webView.getSettings().setSupportZoom(true);
	            webView.getSettings().setBuiltInZoomControls(true);
	            webView.getSettings().setDisplayZoomControls(false);
	            webView.getSettings().setJavaScriptEnabled(true);
	            webView.getSettings().setAllowFileAccess(true);
	            webView.getSettings().setUseWideViewPort(true);
	            webView.getSettings().setLoadWithOverviewMode(true);
	            webView.getSettings().setUserAgentString("Android");
	            webView.setOnReaderViewSwipeListener(new onReaderViewSwipeListener() {
					
					@Override
					public void onSwipeRight() {

						if (bookReader.prevPage()) {

				            try {
				            	String data = new String(bookReader.getPage());

					            webView.loadDataWithBaseURL("file:///" + bookReader.getBaseUrl(), data, "application/xhtml+xml", "utf-8", null);

					            ((TextView) findViewById(R.id.textPage)).setText(
					            		(bookReader.getCurrentPageNumber()+1) + "/" + bookReader.getNumPages());
							} catch (IOException e) {
								Log.e("test-error", e.toString());
								e.printStackTrace();
							}
						}
					}
					
					@Override
					public void onSwipeLeft() {
						if (bookReader.nextPage()) {

				            try {
				            	String data = new String(bookReader.getPage());

					            webView.loadDataWithBaseURL("file:///" + bookReader.getBaseUrl(), data, "application/xhtml+xml", "utf-8", null);

					            
					            ((TextView) findViewById(R.id.textPage)).setText(
					            		(bookReader.getCurrentPageNumber()+1) + "/" + bookReader.getNumPages());
							} catch (IOException e) {
								Log.e("test-error", e.toString());
								e.printStackTrace();
							}
						}
					}
				});
				changeFontSize(50);
				
	            webView.loadDataWithBaseURL("file:///" + bookReader.getBaseUrl(), data, "application/xhtml+xml", "utf-8", null);
			} catch (IOException e) {
				e.printStackTrace();
			}
        } else {
        	Toast.makeText(this, "None", Toast.LENGTH_SHORT).show();
        }

        dataSource = new EPubDataSource(this);

        findViewById(R.id.daynight).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				toggleDayNightMode();
			}
		});
        
        findViewById(R.id.buttonPre).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Toast.makeText(EPubActivity.this, "Zoom = " + webView.getScale(), Toast.LENGTH_SHORT).show();
			}
		});

        findViewById(R.id.button).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Toast.makeText(EPubActivity.this, "X,Y = " + webView.getScrollX() + "," + webView.getScrollY(), Toast.LENGTH_SHORT).show();
			}
		});
        
        ((TextView) findViewById(R.id.textPage)).setText(
        		(bookReader.getCurrentPageNumber()+1) + "/" + bookReader.getNumPages());
    }
	
	int page = 1;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.epub, menu);
        return true;
    }

    public File loadFile(String path) {
        File directory = Environment.getExternalStorageDirectory();
        File file = new File(directory, path);
        
        return file;
    }

    public void prepareTOC(List<TOCReference> tocReferences) {
    	
    }
    
    public void search(String keyword) {
    	webView.findAll(keyword);
    }
    
    public void addBookmark() {
    	int page = bookReader.getCurrentPageNumber();
    	String bookName = bookReader.getTitle();
    	
    	dataSource.createBookmark(bookName, page);
    }
    
    public void deleteBookmark(EPubBookmark bookmark) {
    	int id = bookmark.getId();
    	
    	dataSource.deleteBookmark(id);
    }
    
    public void listBookmark() {
    	
    }
    
    public void addNote(String note) {
    	int page = bookReader.getCurrentPageNumber();
    	String bookName = bookReader.getTitle();
    	
    	dataSource.createNote(bookName, page, note);
    }
    
    public void deleteNote(EPubNote note) {
    	int id = note.getId();
    	
    	dataSource.deleteNote(id);
    }
    
    public void listNote() {
    	
    }
    
    public void copyToClipboard(String text) {
    	
    }
    
    public void invokeGoogleSearch(String query) {
    	Intent searchIntent = new Intent(Intent.ACTION_WEB_SEARCH);
    	searchIntent.putExtra(SearchManager.QUERY, query);
    	
    	Intent intent = Intent.createChooser(searchIntent, "Complete action using");
    	startActivity(intent);
    }
    
    public void invokeWikiSearch(String query) {
    	String url = "http://en.wikipedia.org/wiki/Special:Search?search=" + query;
    	Intent searchIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
    	
    	Intent intent = Intent.createChooser(searchIntent, "Complete action using");
    	startActivity(intent);
    }
    
    public void openDictionary(String query) {
    	String url = "http://www.freedictionary.org/?Query=" + query;
    	Intent searchIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
    	
    	Intent intent = Intent.createChooser(searchIntent, "Complete action using");
    	startActivity(intent);
    }
    
    public static final int MODE_DAY = 0;
    public static final int MODE_NIGHT = 1;
    public int currentMode = MODE_DAY;
    
    public void toggleDayNightMode() {
    	if (currentMode == MODE_DAY) {
    		currentMode = MODE_NIGHT;
    		changeFontColor(Color.WHITE);
    		changeReaderBackground(Color.BLACK);
    	} else {
    		currentMode = MODE_DAY;
    		changeFontColor(Color.BLACK);
    		changeReaderBackground(Color.WHITE);
    	}
    }
    
    public void setOrientation(int orientation) {
    	setRequestedOrientation(orientation);
    }
    
    public void changeReaderBackground(int color) {
    	int r = Color.red(color);
    	int g = Color.green(color);
    	int b = Color.blue(color);
    	
		String javascript="javascript:document.body.style.background = 'rgb(" + r + "," + g + "," + b + ")';";
        webView.loadUrl(javascript);
    }
    
    public void changeFontSize(int size) {
    	WebSettings webSettings = webView.getSettings();
    	
    	webSettings.setDefaultFontSize(size);
    }
    
    public void changeFontStyle(String font) {
    	WebSettings webSettings = webView.getSettings();
    	
    	webSettings.setFixedFontFamily(font);
    }
    
    public void changeFontColor(int color) {
    	int r = Color.red(color);
    	int g = Color.green(color);
    	int b = Color.blue(color);
    	
		String javascript="javascript:document.body.style.color = 'rgb(" + r + "," + g + "," + b + ")';";
        webView.loadUrl(javascript);
    }
    
    public void changeMargin() {
    	
    }
}
