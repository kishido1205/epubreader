package com.rex.epub.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.webkit.WebView;

public class EPubReaderView extends WebView {

	private GestureDetector gestureDetector;
	
	public EPubReaderView(Context context, AttributeSet attribs) {
		super(context, attribs);
		
		this.gestureDetector = new GestureDetector(context, new CustomeGestureDetector());
	}
	
	/* 
	 * @see android.webkit.WebView#onScrollChanged(int, int, int, int)
	 */
	@Override
	protected void onScrollChanged(int l, int t, int oldl, int oldt) {
	    super.onScrollChanged(l, t, oldl, oldt);
	}

	/* 
	 * @see android.webkit.WebView#onTouchEvent(android.view.MotionEvent)
	 */
	@Override
	public boolean onTouchEvent(MotionEvent ev) {
	    return gestureDetector.onTouchEvent(ev) || super.onTouchEvent(ev);
	}
	
	private class CustomeGestureDetector extends SimpleOnGestureListener {      
	    @Override
	    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
	        if (swipeListener == null) {
	        	return false;
	        }
	    	
	    	if(e1 == null || e2 == null) { 
	        	return false;
	        }
	        
	        if(e1.getPointerCount() > 1 || e2.getPointerCount() > 1) {
	        	return false;
	        } else {
	            try {
	            	if (Math.abs(e1.getY() - e2.getY()) > 50) {
	            		Log.w("test", "has y movement");
	            		return false;
	            	}
	            	
	                if(e1.getX() - e2.getX() > 100 && Math.abs(velocityX) > 1000) {
	                    //do your stuff
	                	Log.i("test", "right to left");
	                	swipeListener.onSwipeLeft();
	                    return true;
	                } else if (e2.getX() - e1.getX() > 100 && Math.abs(velocityX) > 1000) {
	                    //do your stuff
	                	Log.i("test", "left to right");
	                	swipeListener.onSwipeRight();
	                    return true;
	                }
	            } catch (Exception e) { // nothing
	            }
	            return false;
	        }
	    }
	}
	
	private onReaderViewSwipeListener swipeListener;
	
	public void setOnReaderViewSwipeListener(onReaderViewSwipeListener listener) {
		this.swipeListener = listener;
	}
	
	public interface onReaderViewSwipeListener {
		
		void onSwipeLeft();
		
		void onSwipeRight();
	}
}
