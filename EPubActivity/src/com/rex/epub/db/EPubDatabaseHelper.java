package com.rex.epub.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class EPubDatabaseHelper extends SQLiteOpenHelper {

	public static final String DATABASE_NAME = "epub";
	public static final int DATABASE_VERSION = 1;
	
	public static final String TABLE_BOOKMARK = "bookmark";
	public static final String TABLE_NOTE = "note";
	
	public static final String COLUMN_ID = "bookmarkId";
	public static final String COLUMN_BOOK = "bookName";
	public static final String COLUMN_PAGE = "bookPage";
	public static final String COLUMN_NOTE = "note";
	
	public EPubDatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE " + TABLE_BOOKMARK + " ("
				+ COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ COLUMN_BOOK + " TEXT,"
				+ COLUMN_PAGE + " INTEGER)");

		db.execSQL("CREATE TABLE " + TABLE_NOTE + " ("
				+ COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ COLUMN_BOOK + " TEXT,"
				+ COLUMN_PAGE + " INTEGER"
				+ COLUMN_NOTE + " TEXT)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_BOOKMARK);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTE);
		onCreate(db);
	}

}
