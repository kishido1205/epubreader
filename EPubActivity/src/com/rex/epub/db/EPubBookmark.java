package com.rex.epub.db;

public class EPubBookmark {

	public int id;
	
	public String book;
	
	public int page;
	
	public EPubBookmark() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBook() {
		return book;
	}

	public void setBook(String book) {
		this.book = book;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}
	
	
}
