package com.rex.epub.db;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class EPubDataSource {

	public SQLiteDatabase database;
	
	public EPubDatabaseHelper helper;
	
	public String[] bookmarkColumns = { 
			EPubDatabaseHelper.COLUMN_ID, 
			EPubDatabaseHelper.COLUMN_BOOK,
			EPubDatabaseHelper.COLUMN_PAGE
	};

	public String[] noteColumns = { 
			EPubDatabaseHelper.COLUMN_ID, 
			EPubDatabaseHelper.COLUMN_BOOK,
			EPubDatabaseHelper.COLUMN_PAGE,
			EPubDatabaseHelper.COLUMN_NOTE,
	};
	
	public EPubDataSource(Context context) {
		helper = new EPubDatabaseHelper(context);
	}
	
	public void open() throws Exception {
		database = helper.getWritableDatabase();
	}
	
	public void close() {
		helper.close();
	}
	
	public void createBookmark(String book, int page) {
		ContentValues values = new ContentValues();
		values.put(EPubDatabaseHelper.COLUMN_BOOK, book);
		values.put(EPubDatabaseHelper.COLUMN_PAGE, page);
		
		database.insert(EPubDatabaseHelper.TABLE_BOOKMARK, null, values);
	}
	
	public void deleteBookmark(int id) {
		database.delete(EPubDatabaseHelper.TABLE_BOOKMARK, EPubDatabaseHelper.COLUMN_ID + " = " + id, null);
	}
	
	public List<EPubBookmark> getAllBookmark() {
		List<EPubBookmark> bookmarks = new ArrayList<EPubBookmark>();
		
		Cursor cursor = database.query(EPubDatabaseHelper.TABLE_BOOKMARK,
				bookmarkColumns, null, null, null, null, null);
		
		cursor.moveToFirst();
	    while (!cursor.isAfterLast()) {
	      EPubBookmark bookmark = cursorToBookmark(cursor);
	      bookmarks.add(bookmark);
	      cursor.moveToNext();
	    }
	    // make sure to close the cursor
	    
	    cursor.close();
		return bookmarks;
	}

	private EPubBookmark cursorToBookmark(Cursor cursor) {
		EPubBookmark bookmark = new EPubBookmark();
		
		bookmark.setId(cursor.getInt(0));
		bookmark.setBook(cursor.getString(1));
		bookmark.setPage(cursor.getInt(2));
		
		return bookmark;
	}
	
	public void createNote(String book, int page, String note) {
		ContentValues values = new ContentValues();
		values.put(EPubDatabaseHelper.COLUMN_BOOK, book);
		values.put(EPubDatabaseHelper.COLUMN_PAGE, page);
		values.put(EPubDatabaseHelper.COLUMN_NOTE, note);
		
		database.insert(EPubDatabaseHelper.TABLE_NOTE, null, values);
	}
	
	public void deleteNote(int id) {
		database.delete(EPubDatabaseHelper.TABLE_NOTE, EPubDatabaseHelper.COLUMN_ID + " = " + id, null);
	}
	
	public List<EPubNote> getAllNotes() {
		List<EPubNote> notes = new ArrayList<EPubNote>();
		
		Cursor cursor = database.query(EPubDatabaseHelper.TABLE_BOOKMARK,
				bookmarkColumns, null, null, null, null, null);
		
		cursor.moveToFirst();
	    while (!cursor.isAfterLast()) {
	      EPubNote note = cursorToNote(cursor);
	      notes.add(note);
	      cursor.moveToNext();
	    }
	    // make sure to close the cursor
	    
	    cursor.close();
		return notes;
	}

	private EPubNote cursorToNote(Cursor cursor) {
		EPubNote note = new EPubNote();
		
		note.setId(cursor.getInt(0));
		note.setBook(cursor.getString(1));
		note.setPage(cursor.getInt(2));
		note.setNote(cursor.getString(3));
		
		return note;
	}
}
