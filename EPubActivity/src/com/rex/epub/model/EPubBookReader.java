package com.rex.epub.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import nl.siegmann.epublib.domain.Book;
import nl.siegmann.epublib.domain.Resource;
import nl.siegmann.epublib.domain.Spine;
import nl.siegmann.epublib.domain.SpineReference;
import nl.siegmann.epublib.epub.EpubReader;
import android.os.Environment;
import android.util.Log;

public class EPubBookReader {

    private Book book;
    
    private int currentPageNumber;
    private int pageCount;

	private Resource tableOfContent;
	private List<SpineReference> spineRefs;
	
	private File bookDir;
    
	private Map<String, String> baseUrls;
	
    public EPubBookReader(File file) {
    	
    	try {
			book = new EpubReader().readEpub(new FileInputStream(file));
		} catch (IOException e) {
			e.printStackTrace();
		}
    	
    	Spine spine = book.getSpine();
    	
    	spineRefs = spine.getSpineReferences();
    	tableOfContent = spine.getTocResource();
    	
    	book.setSpine(spine);
    	
    	currentPageNumber = 0;
    	pageCount = spineRefs.size();
    	
    	unzip(file);
    	
    	prepareBaseUrls();
    }
    
	private void prepareBaseUrls() {
		baseUrls = new HashMap<String, String>();
    	
		getXHTMLFile(bookDir);
	}

	private void getXHTMLFile(File file) {
		if (file.isDirectory()) {
			File[] files = file.listFiles();
			int length = files.length;
			for (int i=0; i<length; i++) {
				File temp = files[i];
				
				if (temp.isDirectory()) {
					getXHTMLFile(temp);
				} else {
					String path = temp.getAbsolutePath();
					String name = temp.getName();
					if (name.endsWith(".xhtml")) {
						String value = path.substring(0, path.lastIndexOf(name));
						baseUrls.put(name, value);
					}
				}
			}
		}
	}

	private void unzip(File file) {
		String path = Environment.getExternalStorageDirectory().getPath();
		
		// create directory
		File topDir = new File(path + "/epub/");
		if (!topDir.exists()) {
			topDir.mkdir();
		} else if (!topDir.isDirectory()) {
			topDir.mkdir();
		}
		
		// create directory for the book
		bookDir = new File(path + "/epub/" + file.getName());
		if (!bookDir.exists()) {
			bookDir.mkdir();
		} else if (!bookDir.isDirectory()) {
			bookDir.mkdir();
		}
		
		ZipFile zip = null;
		
		// unzip file to the directory
		try {
			zip = new ZipFile(file, ZipFile.OPEN_READ);
			Enumeration<?> entries = zip.entries();
			
			while (entries.hasMoreElements()) {
				ZipEntry entry = (ZipEntry) entries.nextElement();
				
				mkdir(bookDir, entry.getName());
				
				if (entry.isDirectory()) {
					File entryFile = new File(bookDir, entry.getName());
					entryFile.mkdir();
				} else {
					File entryFile = new File(bookDir, entry.getName());
					FileOutputStream fos = new FileOutputStream(entryFile);
					InputStream is = zip.getInputStream(entry);
					byte[] buffer = new byte[2048];
					int bytesRead = 0;
					while ((bytesRead = is.read(buffer)) != -1) {
	                    fos.write(buffer, 0, bytesRead);
	                }
	                fos.close();
				}
			}
			
			zip.close();
		} catch (ZipException e) {
			e.printStackTrace();
			Log.w("test", e.toString());
		} catch (IOException e) {
			e.printStackTrace();
			Log.w("test", e.toString());
		}
	}
	
	private void mkdir(File root, String entry) {
		String[] splits = entry.split("/");
		int len = splits.length;
		
		if (len <= 1) {
			return;
		}
		
		String addon = "";
		
		for (int i=0; i<len-1; i++) {
			File temp = new File(root + addon, splits[i]);
			temp.mkdir();
			addon = "/" + splits[i];
		}
	}

	public int getCurrentPageNumber() {
		return currentPageNumber;
	}
	
	public void setCurrentPageNumber(int page) {
		this.currentPageNumber = page;
	}
	
	public String getTitle() {
		return book.getTitle();
	}

	public byte[] getPage() throws IOException {
		return book.getContents().get(currentPageNumber).getData();
	}
	
	public String getPageHref() {
		return book.getContents().get(currentPageNumber).getHref();
	}
	
	public boolean nextPage() {
		int size = book.getContents().size();
		
		if (currentPageNumber + 1 < size) {
			currentPageNumber++;
			return true;
		}
		
		return false;
	}
	
	public boolean prevPage() {
		if (currentPageNumber > 0) {
			currentPageNumber--;
			return true;
		}
		
		return false;
	}
	
	public Resource getTableOfContent() {
		return this.tableOfContent;
	}
	
	public int getNumPages() {
		return this.pageCount;
	}
	
	public String getBaseUrl() {
		String href = book.getContents().get(currentPageNumber).getHref();
		String clean = href.substring(href.lastIndexOf("/")+1);
		
		return baseUrls.get(clean);
	}

	public void validate(String url) {
		Log.w("test", "URL = " + url);
		
		int page = 0;
		
		List<Resource> res = book.getContents();
		int size = res.size();
		
		for (int i=0; i<size; i++) {
			Log.w("test", "page = " + res.get(i).getHref());
			if (res.get(i).getHref().endsWith(url.substring(url.lastIndexOf("/")))) {
				break;
			}
			page++;
		}
		
		currentPageNumber = page;
	}
}
