package com.rex.epub.model;

import java.io.File;

public class EPubPage {

    public File htmlFile;
    
    public int page;
    
    public String id;
    
    public String url;
    
    public EPubPage() {
        // do nothing
    }

    public File getHtmlFile() {
        return htmlFile;
    }

    public void setHtmlFile(File htmlFile) {
        this.htmlFile = htmlFile;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

	public String getPath() {
		return htmlFile.getPath();
	}
}
